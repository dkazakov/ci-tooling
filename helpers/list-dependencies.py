#!/usr/bin/python3
import os
import sys
import argparse
from helperslib import BuildSpecs, CommonUtils, Buildable, Packages

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to execute tests for a build.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--branchGroup', type=str, default='kf5-qt5')
parser.add_argument('--platform', type=str, required=True)
parser.add_argument('--withProject', action='store_true', help="include the requested project in the list")
parser.add_argument('projects', nargs='+', help='projects to see the dependencies from', type=str)
arguments = parser.parse_args()

# Initialise the dependnecy resolver
resolver = Buildable.DependencyResolver()

# Ask the resolver to load the list of projects...
projectsTreeLocation = os.path.join( CommonUtils.scriptsBaseDirectory(), 'repo-metadata', 'projects-invent' )
resolver.loadProjectsFromTree( projectsTreeLocation )

# Now ask it to load the list of projects to ignore
ignoreFileLocation = os.path.join( CommonUtils.scriptsBaseDirectory(), 'repo-metadata', 'dependencies', 'build-script-ignore' )
resolver.loadProjectsIgnoreList( ignoreFileLocation )
# As well as our local ignore file
ignoreFileLocation = os.path.join( CommonUtils.scriptsBaseDirectory(), 'local-metadata', 'ignored-projects' )
resolver.loadProjectsIgnoreList( ignoreFileLocation )

# Make sure the Platform specific ignore rules are loaded as well
ignoreRulesLocation = os.path.join( CommonUtils.scriptsBaseDirectory(), 'local-metadata', 'project-ignore-rules.yaml' )
resolver.loadProjectsIgnoreRules( ignoreRulesLocation )

# Now get it to load the list of dependencies - we have first the common file...
dependenciesFile = os.path.join( CommonUtils.scriptsBaseDirectory(), 'repo-metadata', 'dependencies', 'dependency-data-common' )
resolver.loadDependenciesFromFile( dependenciesFile )
# And then the branch group specific file...
dependenciesFile = os.path.join( CommonUtils.scriptsBaseDirectory(), 'repo-metadata', 'dependencies', 'dependency-data-' + arguments.branchGroup )
resolver.loadDependenciesFromFile( dependenciesFile )

# If it exists, we should load an OS specific file as well
dependenciesFile = os.path.join( CommonUtils.scriptsBaseDirectory(), 'local-metadata', 'dependency-' + sys.platform )
if os.path.exists( dependenciesFile ):
    resolver.loadDependenciesFromFile( dependenciesFile )

projectsDependencies = []
for project in arguments.projects:
    # Now that we are all initialised, we can ask the resolver for the project we represent, then resolve it's dependencies
    resolverProject = resolver.retrieveProject( project )
    projectsDependencies.extend(resolver.forProject( resolverProject, arguments.platform ))
    if arguments.withProject and not resolverProject.virtualDependency:
        projectsDependencies.append(resolverProject)

# Iterate over them and print details
for project in resolver.unique(projectsDependencies):
    print( project.identifier )
